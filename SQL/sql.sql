CREATE DATABASE userdb DEFAULT CHARACTER SET utf8;
USE userdb;
CREATE TABLE user (

                    id SERIAL PRIMARY KEY AUTO_INCREMENT,
                    login_id varchar(255) UNIQUE NOT NULL,
                    name varchar(255) NOT NULL,
                    birth_date DATE NOT NULL,
					password varchar(255) NOT NULL,
                    create_date DATETIME NOT NULL,
                    update_date DATETIME NOT NULL
                    );

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)
			 VALUES('admin','�Ǘ���','1994-07-06','password',NOW(),NOW());
